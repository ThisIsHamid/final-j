package Dark;

import Logic.Song;
import Logic.playList;
import MainPackage.MyColors;
import MainPackage.MyFonts;
import MainPackage.MyIcons;
//import com.sun.deletePlaylistTitleoy.panel.JavaPanel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DarkHomePlaylistsItemsForSelecting extends JButton implements ActionListener {

    JLabel songName;
    JLabel artwork;
    public playList playlist;
    Song song;
    playList playList;


    public DarkHomePlaylistsItemsForSelecting(playList playList){
        super();
        setPreferredSize(new Dimension(150, 126));

        this.playlist = playList;
//        this.song = song;

        setBorderPainted(false);
        addActionListener(this);

        artwork = new JLabel();
        songName = new JLabel();
        setLayout(new BorderLayout());
        setBackground(MyColors.DarkBackground);
        artwork.setMaximumSize(new Dimension(100,100));
        artwork.setMinimumSize(new Dimension(100,100));
        artwork.setSize(100,100);
        artwork.setBackground(MyColors.DarkBackground);

        artwork.setIcon(MyIcons.DarkNoArtworkSmall);
        artwork.setHorizontalAlignment(JLabel.CENTER);

        add(artwork , BorderLayout.CENTER);

        JPanel musicInfo = new JPanel(new BorderLayout());
        musicInfo.setBackground(MyColors.DarkBackground);
        songName.setText(playList.getName());
        songName.setSize(100,50);
        songName.setMinimumSize(new Dimension(100,50));
        songName.setMaximumSize(new Dimension(100,50));
        songName.setBorder(new EmptyBorder(0,0,0,10));
        songName.setHorizontalAlignment(JLabel.CENTER);
        songName.setFont(MyFonts.arialBold);
        songName.setForeground(MyColors.DarkTextColor);
        songName.setBackground(MyColors.DarkBackground);




        musicInfo.add(songName , BorderLayout.NORTH);
        add(musicInfo , BorderLayout.SOUTH);


        this.playlist = playList;
    }




    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==this){
//            Main.darkFrame.setAllMainPanelsInvisible();
//            Main.darkFrame.addOnePlaylistPanelToTheMainPanel(this.playlist);
//            this.playlist.getPlayListSongsFrame().setVisible(true);
//            this.playlist.addSongs(song);

        }
    }

}
