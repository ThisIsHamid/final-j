package Dark;

import MainPackage.MyColors;
import MainPackage.MyIcons;

import javax.swing.*;
import java.awt.*;

public class DarkScrollBarLeft extends JPanel {
    public DarkScrollBarLeft(){
        super();
        setBackground(MyColors.DarkFooter);
        setLayout(new BorderLayout());

//        JPanel headerMusicLogo = new JPanel(new BorderLayout());// for the music logo and "JPOTIFY" title...
//        JLabel musicLogo = new JLabel(MyIcons.DarkJpotifyLogo);
//        headerMusicLogo.add(musicLogo,BorderLayout.CENTER);
//        add(headerMusicLogo , BorderLayout.PAGE_START);

        JPanel artworkPanel = new JPanel();// for showing music artwork
        artworkPanel.setBackground(Color.cyan);
        JLabel artwork = new JLabel();
        artwork.setIcon(MyIcons.DarkNoArtwork);
        artworkPanel.add(artwork , BorderLayout.CENTER);
        add(artwork, BorderLayout.PAGE_END);
    }
}
