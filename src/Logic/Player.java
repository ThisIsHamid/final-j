package Logic;
import Dark.*;
import MainPackage.ColoredThumbSliderUI;
import MainPackage.Main;
import MainPackage.MyColors;
import MainPackage.MyIcons;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.*;
import com.mpatric.mp3agic.UnsupportedTagException;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.advanced.AdvancedPlayer;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Player {

    public ArrayList<Song> songs = new ArrayList<>();
    private ArrayList<Song> currentPlayList ;
    private javazoom.jl.player.advanced.AdvancedPlayer player;
    FileInputStream fileInputStream;
    private static Song currentSong; // sample file;
    Thread playThread ;
    Thread timeThread;
    private int currentTime = 0;
    public JSlider timeLine;
    public JLabel songLength;
    public JLabel currentTimepanel;
    public static HashMap<String , ArrayList<Song>> albumsHashMap;
    public ArrayList<DarkAlbumSongsFrame> albumFrames;

    public static ArrayList<playList> playLists = new ArrayList<>(); // arrayList of playLists
//    public ArrayList<playList> liked = new ArrayList<>(); // arrayList of liked songs
//    public ArrayList<playList> shared = new ArrayList<>(); // arrayList of shares songs

    public static playList sharedPlaylist= new playList("Shared Playlist" , "share your music with your friends:)");
    public static playList favouritePlaylist= new playList("Favourite Playlist" , "favourite songs...");



    BufferedInputStream bufferedInputStream;

    public static void addSongToPlayList(Song song, playList pl){
//        pl.addSongs(song);
//        Main.darkFrame.addPlaylistsToMainPanel();


        System.out.println(song.getName() + " added to " + pl.getName());
        System.out.println(pl.getSongs().size() + " songs in \""+ pl.getName()+"\" playlist");

    }

    public static void removeSongFromPlayList(Song song, playList pl){
        pl.removeSongs(song);
        System.out.println(song.getName() + " added to " + pl.getName());
        System.out.println(pl.getSongs().size() + " songs in \""+ pl.getName()+"\" playlist");
    }

    public ArrayList<Song> getSongs() {
        return songs;
    }

    public static ArrayList<playList> getPlayLists() {
        return playLists;
    }

    public Thread getPlayThread() {
        return playThread;
    }


    public Player() {

        currentPlayList = songs;
        playThread = new Thread(runnableplay);
        timeLine = new JSlider();
        timeLine.setUI(new ColoredThumbSliderUI(timeLine , MyColors.DarkTextColor));
        currentTimepanel = new JLabel();
        songLength = new JLabel();
        albumsHashMap = new HashMap<>();
        timeLine.setValue(0);




        timeThread = new Thread(time);
        timeThread.setPriority(4);
        timeThread.start();

    }

    public static void addOrCreateAlbum(Song song){
        System.out.println("trying to create a album...");
        if (!albumsHashMap.containsKey(song.getAlbum())) {//if the album has not been created before then creat a new one!
            albumsHashMap.put( song.getAlbum(), new ArrayList<>());
            System.out.println("album "+ song.getAlbum() +" created!");
            albumsHashMap.get(song.getAlbum()).add(song);

            JFrame sss = new DarkAlbumSongsFrame(song.getAlbum() , song.getArtist() , song);


        }else{//if the album has  been created before then add your song to it!
            albumsHashMap.get(song.getAlbum()).add(song);
        }
        System.out.println("song "+song.getName()+" added to album " + song.getAlbum());
        System.out.println("total albums: "+ albumsHashMap.size());
        System.out.println(albumsHashMap.get(song.getAlbum()).size() + "songs in "+ song.getAlbum());
    }

    Runnable runnableplay = new Runnable() {

        @Override
        public void run() {
            boolean emptyPlayList = false;
            if (currentPlayList.size() == 0) emptyPlayList = true;
            while (emptyPlayList) {
                // waiting until playlist has some shitz to play   -_-
            }
//            do {            // if replayAllOrNot is true , we want to replay all , so this do-while loop will be repeated . else it will be done once at least
//                if (currentPlayList.indexOf(currentSong) != currentPlayList.size()-1)
//                    setCurrentSong(currentPlayList.get(currentPlayList.indexOf(currentSong) + 1));
//                else setCurrentSong(currentPlayList.get(0));
//                do {        // if replayAllOrNot is false , we want to replay one , so this do-while loop will be repeated . else it will be done once at least

                    try {
                        if (currentSong != null) {
                            System.out.println(currentSong.getName() + "is now playing");
//                    DarkFooter.setDarkMusicInfo(new DarkMusicInfo(currentSong));
                            DarkFooter.setDarkMusicInfo(currentSong.getName(), currentSong.getArtist());
                            DarkLeftSidePanel.artwork.setIcon(currentSong.getArtWork());

                            fileInputStream = new FileInputStream(currentSong.getFile());
                            bufferedInputStream = new BufferedInputStream(fileInputStream);
                            System.out.println(currentSong.getName());
                            playThread.setPriority(1);
                            player = new javazoom.jl.player.advanced.AdvancedPlayer(fileInputStream);
                            DarkMusicController.passedTime = (player.getPosition() / 1000);


                        }else setCurrentSong(currentPlayList.get(0));
                        try {

                            player.play(currentSong.getMp3File().getFrameCount());
                        }catch (NullPointerException e){
                            System.out.println("null player");
                        }
//                        while (player)

                    } catch(Exception e){
                        e.printStackTrace();
                    }

            if (DarkControlButtons.replayAllOrNot){
                getCurrentSong().getDarkSongPanel().playOrPaused = false;
                getCurrentSong().getDarkSongPanel().playAndPause.setIcon(MyIcons.DarkPlaySmall);
                if (currentPlayList.indexOf(currentSong) == (currentPlayList.size()-1)){
                    setCurrentSong(currentPlayList.get(0));
                }else setCurrentSong(currentPlayList.get(currentPlayList.indexOf(currentSong) + 1));

            } else {
                setCurrentSong(currentSong);
            }
//                }while (!DarkControlButtons.replayAllOrNot);
//
//            } while (DarkControlButtons.replayAllOrNot);
        }
    };

    public static Song getCurrentSong() {
        return currentSong;
    }

    public void setCurrentPlayList(ArrayList<Song> currentPlayList) {
        this.currentPlayList = currentPlayList;
    }

    public ArrayList<Song> getCurrentPlayList() {
        return currentPlayList;
    }

    public void setCurrentSong(Song currentSong) {

        Player.currentSong = currentSong;
        if (playThread.isAlive())
            playThread.interrupt();
        playThread = new Thread(runnableplay);
        System.out.println(currentSong.getName());
//        DarkFooter.add(currentSong.getDarkMusicInfo());
        songLength.setText(currentSong.getLength());
//        DarkMainPanel.setPlayingSongPanel();


        playThread.start();

    }

    public javazoom.jl.player.advanced.AdvancedPlayer getPlayer() {
        return player;
    }


    //    public void setVol(float volume){
//        player.setVol(volume);
//    }
    Runnable time = new Runnable() {

        @Override
        public void run() {
            while (true) {

                if (playThread.isAlive() && player != null ) {
                    timeLine.setMaximum((int) currentSong.getMp3File().getLengthInMilliseconds());
                    currentTime = player.getPosition();
                    timeLine.setValue(currentTime);
                    currentTimepanel.setText((currentTime/1000)/60 + ":" + (currentTime/1000)%60);
                    songLength.setText((currentSong.getMp3File().getLengthInSeconds() - currentTime/1000)/60 +":"+(currentSong.getMp3File().getLengthInSeconds() - currentTime/1000)%60);
//                    if (timeLine.getValue() != currentTime){
//                        try {
//                            seek(timeLine.getValue());
//                        } catch (JavaLayerException e) {
//                            e.printStackTrace();
//                        }
//                    }

                }
            }
        }
    };

    public static playList getSharedPlaylist() {
        return sharedPlaylist;
    }

    public static playList getFavouritePlaylist() {
        return favouritePlaylist;
    }

//    public void seek(int startingFrame) throws JavaLayerException {
//        playThread.suspend();
//
//        player = new javazoom.jl.player.advanced.AdvancedPlayer(fileInputStream);
//        player.play(startingFrame , currentSong.getMp3File().getFrameCount());
//        playThread.resume();
//    }
//    ChangeListener changeListener = new ChangeListener() {
//        @Override
//        public void stateChanged(ChangeEvent e) {
//            try {
//                seek(timeLine.getValue());
//            } catch (JavaLayerException e1) {
//                e1.printStackTrace();
//            }
//        }
//    };
}


