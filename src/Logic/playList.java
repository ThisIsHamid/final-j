package Logic;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import Dark.*;
import MainPackage.*;


public class playList {
    private static String name; //name of playlist
    private String description; //description
    private static ArrayList<Song> Songs = new ArrayList<>(); // songs of playlist
    private static ImageIcon artWork;
    private static DarkMainPlaylistPanel mainPlaylistPanel;
    private DarkPlaylistTitle playlistTitle;
    private DarkHomePlaylistsItems homePlaylistsItemsForHome;
    private DarkHomePlaylistsItems homePlaylistsItemsForPlayListsPanel;
    private DarkHomePlaylistsItemsForSelecting homePlaylistsItemsForSelectingPlayListFrame;
    private static DarkPlayListSongsFrame playListSongsFrame;
    private static JMenuItem playListMenuItemForaddingSong;

    public DarkHomePlaylistsItemsForSelecting getHomePlaylistsItemsForSelectingPlayListFrame() {
        return homePlaylistsItemsForSelectingPlayListFrame;
    }

    public static JMenuItem getPlayListMenuItemForaddingSong() {
        return playListMenuItemForaddingSong;
    }

    public static DarkPlayListSongsFrame getPlayListSongsFrame() {
        return playListSongsFrame;
    }

    public DarkPlaylistTitle getPlaylistTitle() {
        return playlistTitle;
    }

    public DarkHomePlaylistsItems getHomePlaylistsItems() {
        return homePlaylistsItemsForHome;
    }

    public DarkHomePlaylistsItems getHomePlaylistsItemsForPlayListsPanel() {
        return homePlaylistsItemsForPlayListsPanel;
    }

    public static DarkMainPlaylistPanel getMainPlaylistPanel() {
        return mainPlaylistPanel;
    }

    public playList(String name, String description) {
        this.name = name;
        this.description = description;
        artWork = MyIcons.DarkNoArtwork;
        mainPlaylistPanel = new DarkMainPlaylistPanel(this);
        playlistTitle = new DarkPlaylistTitle(this);
//        homePlaylistsItemsForHome = new DarkHomePlaylistsItems(this);
        homePlaylistsItemsForHome = new DarkHomePlaylistsItems(this);
        homePlaylistsItemsForPlayListsPanel = new DarkHomePlaylistsItems(this);
        homePlaylistsItemsForSelectingPlayListFrame = new DarkHomePlaylistsItemsForSelecting(this);
//        System.out.println(homePlaylistsItemsForSelectingPlayListFrame.playlist.getDescription());
        playListSongsFrame = new DarkPlayListSongsFrame(this);
        playListMenuItemForaddingSong = new JMenuItem(new AbstractAction("Add to PlayList") {
            public void actionPerformed(ActionEvent e) {
                try {
                    //
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }
        });    }

    public static String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Song> getSongs() {
        return Songs;
    }

    public static void addSongs(Song song) {
        Songs.add(song);
        System.out.println("a song is added");
        if (song.getArtWork() != null)
            artWork = song.getArtWork();
        else artWork = MyIcons.DarkNoArtwork;

        getPlayListSongsFrame().getSongsList().add(song.getDarkSongPanelForPlaylistMainPanel() );
        getPlayListSongsFrame().getSongsList().setVisible(false);
        getPlayListSongsFrame().getSongsList().setVisible(true);
        getPlayListSongsFrame().setVisible(false);
        getPlayListSongsFrame().setVisible(true);

    }

    public void removeSongs (Song song){
        Songs.remove(song);
//        getMainPlaylistPanel().getSongsList().add(song.getDarkSongPanel());
        getPlayListSongsFrame().getSongsList().remove(song.getDarkSongPanelForPlaylistMainPanel() );
        getPlayListSongsFrame().getSongsList().setVisible(false);
        getPlayListSongsFrame().getSongsList().setVisible(true);
        getPlayListSongsFrame().setVisible(false);
        getPlayListSongsFrame().setVisible(true);
    }


}
