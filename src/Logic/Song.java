package Logic;

import Dark.DarkHomeSongsItems;
import Dark.DarkMusicInfo;
import Dark.DarkSongPanel;
import MainPackage.MyIcons;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Iterator;

public class Song {

    private String name;
    private String Artist;
    private String path;
    private String album;
    private boolean hasId3v1Tag;
    private boolean hasId3v2Tag ;
    private Mp3File mp3File;
    private File file;
    private ImageIcon artWork;
    private ImageIcon artWorkSmall;
    DarkSongPanel darkSongPanel ;
    DarkSongPanel darkSongPanelForPlaylistMainPanel;
    DarkSongPanel darkSongPanelForAlbumMainPanel;
    private boolean liked ;
    private boolean shared;
    private String length;
    private DarkMusicInfo darkMusicInfo;
    private DarkHomeSongsItems darkHomeItems;



    public Song(String path , String name) throws Exception {


        this.path = path;
        this.mp3File = new Mp3File(path);
        this.file = new File(path);
        this.hasId3v1Tag = mp3File.hasId3v1Tag();
        this.hasId3v2Tag = mp3File.hasId3v2Tag();
        this.hasId3v1Tag = mp3File.hasId3v1Tag();
        this.hasId3v2Tag = mp3File.hasId3v2Tag();
        this.length = (mp3File.getLengthInSeconds()/60)+ ":"+(mp3File.getLengthInSeconds()%60);
        System.out.println(length);
        this.name = name;
        if (hasId3v1Tag){
            this.album = mp3File.getId3v1Tag().getAlbum();

            System.out.println(name);
            this.Artist = mp3File.getId3v1Tag().getArtist();
        }else{
            this.album = "unknown" ;
            this.Artist = "unknown" ;
        }

        if (hasId3v2Tag){

            byte[] bytes = mp3File.getId3v2Tag().getAlbumImage(); // Your image bytes
            if (mp3File.hasId3v1Tag()) {
                if (mp3File.getId3v1Tag().getArtist() != null && mp3File.getId3v1Tag().getAlbum() != null) {
                    System.out.println("Artist : " + mp3File.getId3v1Tag().getArtist() + " . Album : " + mp3File.getId3v1Tag().getAlbum());
                }
            }

            //resizing artwork to 200*200
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            Iterator<?> readers = ImageIO.getImageReadersByFormatName("jpg");

            //ImageIO is a class containing static methods for locating ImageReaders
            //and ImageWriters, and performing simple encoding and decoding.

            ImageReader reader = (ImageReader) readers.next();
            Object source = bis;
            ImageInputStream iis = ImageIO.createImageInputStream(source);
            reader.setInput(iis, true);
            ImageReadParam param = reader.getDefaultReadParam();
            try  {
                Image srcImg = reader.read(0, param);
                BufferedImage resizedImg = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
                Graphics2D g2 = resizedImg.createGraphics();

                g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                g2.drawImage(srcImg, 0, 0, 200, 200, null);
                g2.dispose();
                artWork = new ImageIcon(resizedImg);
            }catch (Exception e){
                artWork = MyIcons.DarkNoArtwork;
            }



            try  {
                Image srcImgSmall = reader.read(0, param);
                BufferedImage resizedImgSmall = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
                Graphics2D g2Small = resizedImgSmall.createGraphics();

                g2Small.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                g2Small.drawImage(srcImgSmall, 0, 0, 100, 100, null);
                g2Small.dispose();
                artWorkSmall = new ImageIcon(resizedImgSmall);
            }catch (Exception e){
                artWorkSmall = MyIcons.DarkNoArtworkSmall;
            }

            darkSongPanel = new DarkSongPanel(this);
            darkSongPanelForPlaylistMainPanel = new DarkSongPanel(this);
            darkMusicInfo = new DarkMusicInfo(this);
            darkHomeItems = new DarkHomeSongsItems(this);
            darkSongPanelForAlbumMainPanel  = new DarkSongPanel(this);

        }






    }

    public Mp3File getMp3File() {
        return mp3File;
    }

    public void setMp3File(Mp3File mp3File) {
        this.mp3File = mp3File;
    }

    public String getName() {
        return name;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public boolean isHasId3v1Tag() {
        return hasId3v1Tag;
    }

    public void setHasId3v1Tag(boolean hasId3v1Tag) {
        this.hasId3v1Tag = hasId3v1Tag;
    }

    public boolean isHasId3v2Tag() {
        return hasId3v2Tag;
    }

    public void setHasId3v2Tag(boolean hasId3v2Tag) {
        this.hasId3v2Tag = hasId3v2Tag;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getArtist() {
        return Artist;
    }

    public void setArtist(String artist) {
        Artist = artist;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public ImageIcon getArtWork() {
        return artWork;
    }

    public void setArtWork(ImageIcon artWork) {
        this.artWork = artWork;
    }

    public String getLength() {
        return length;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public boolean getLiked() {
        return liked;
    }


    public void setShared(boolean shared) {
        this.shared = shared;
    }

    public boolean getShared() {
        return shared;
    }

    public DarkSongPanel getDarkSongPanel() {
        return darkSongPanel;
    }

    public DarkSongPanel getDarkSongPanelForPlaylistMainPanel() {
        return darkSongPanelForPlaylistMainPanel;
    }

    public DarkSongPanel getDarkSongPanelForAlbumMainPanel() {
        return darkSongPanelForAlbumMainPanel;
    }

    public DarkMusicInfo getDarkMusicInfo() {
        return darkMusicInfo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DarkHomeSongsItems getDarkHomeSongsItems() {
        return darkHomeItems;
    }

    public ImageIcon getArtWorkSmall() {
        return artWorkSmall;
    }

}
