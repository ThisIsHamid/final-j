package Light;

import MainPackage.MyColors;
import MainPackage.MyIcons;

import javax.swing.*;
import java.awt.*;

public class LightControlButtons extends JPanel {
    public LightControlButtons(){
        super();
        setLayout(new FlowLayout());
        setBackground(MyColors.LightFooter);

        JButton shuffle_off = new JButton(MyIcons.LightShuffleOff);
        shuffle_off.setBackground(MyColors.LightFooter);
        shuffle_off.setSize(50,50);
        shuffle_off.setBorderPainted(false);
        add(shuffle_off , BorderLayout.CENTER);

        JButton previous = new JButton(MyIcons.LightPrevious);
        previous.setBackground(MyColors.LightFooter);
        previous.setSize(50,50);
        previous.setBorderPainted(false);
        add(previous , BorderLayout.CENTER);

        JButton play = new JButton(MyIcons.LightPlay);
        play.setBackground(MyColors.LightFooter);
        play.setSize(50,50);
        play.setBorderPainted(false);
        add(play , BorderLayout.CENTER);

        JButton next = new JButton(MyIcons.LightNext);
        next.setBackground(MyColors.LightFooter);
        next.setSize(50,50);
        next.setBorderPainted(false);
        add(next , BorderLayout.CENTER);

        JButton replay_all = new JButton(MyIcons.LightReplayAll);
        replay_all.setBackground(MyColors.LightFooter);
        replay_all.setSize(50,50);
        replay_all.setBorderPainted(false);
        add(replay_all , BorderLayout.CENTER);
    }
}
