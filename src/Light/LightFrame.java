
package Light;

import MainPackage.MyColors;

import javax.swing.*;
import java.awt.*;

public class LightFrame extends JFrame {
//    public static Color LightFooter = new Color(205,205,205);
    public LightFrame(){
        super("JPOTIFY-Light");
        setBackground(MyColors.LightBackground);
        setSize(700,500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        LightFooter footer = new LightFooter();
        add(footer, BorderLayout.SOUTH);

        setVisible(true);
    }
}
