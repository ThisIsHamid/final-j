
package Light;

import javax.swing.*;
import java.awt.*;

public class LightMusicController extends JPanel {
    public LightMusicController(){
        super();
        setLayout(new BorderLayout());
        LightControlButtons buttons = new LightControlButtons();
        add(buttons , BorderLayout.CENTER);
        JPanel timeLine = new JPanel();
        timeLine.setBackground(Color.RED);
        add(timeLine , BorderLayout.SOUTH);
    }
}
