package Light;

import MainPackage.MyColors;

import javax.swing.*;
import java.awt.*;

public class LightFooter extends JPanel {
    public LightFooter(){
        super();
        setBackground(MyColors.LightFooter);
        LightMusicController musicController = new LightMusicController();
        add(musicController, BorderLayout.CENTER);
    }
}
