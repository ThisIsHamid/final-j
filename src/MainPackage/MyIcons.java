package MainPackage;

import javax.swing.*;


public class MyIcons {
    //light mode:
    public static ImageIcon LightShuffleOff = new ImageIcon("icons\\50x50\\light\\shuffle_off.png");
    public static ImageIcon LightShuffleOn = new ImageIcon("icons\\50x50\\light\\shuffle_on.png");
    public static ImageIcon LightPlay = new ImageIcon("icons\\50x50\\light\\play.png");
    public static ImageIcon LightNext = new ImageIcon("icons\\50x50\\light\\next.png");
    public static ImageIcon LightPrevious = new ImageIcon("icons\\50x50\\light\\previous.png");
    public static ImageIcon LightReplayAll = new ImageIcon("icons\\50x50\\light\\replay_all.png");
    public static ImageIcon LightVolume = new ImageIcon("icons\\50x50\\light\\volume.png");
    public static ImageIcon LightAdd = new ImageIcon("icons\\50x50\\light\\add.png");
    public static ImageIcon LightMusic = new ImageIcon("icons\\50x50\\light\\music.png");
    public static ImageIcon LightPause = new ImageIcon("icons\\50x50\\light\\pause.png");
    public static ImageIcon LightPlaylist = new ImageIcon("icons\\50x50\\light\\playlist.png");
    public static ImageIcon LightReplayOnce = new ImageIcon("icons\\50x50\\light\\replay_once.png");
    public static ImageIcon LightStop = new ImageIcon("icons\\50x50\\light\\stop.png");
    public static ImageIcon LightJpotifyLogo = new ImageIcon("icons\\50x50\\light\\header.png");
    public static ImageIcon LightNoArtwork = new ImageIcon("icons\\50x50\\light\\no_artwork.png");
    public static ImageIcon LightMagnifier = new ImageIcon("icons\\50x50\\light\\magnifier.png");
    public static ImageIcon LightPlaylistButton = new ImageIcon("icons\\50x50\\light\\playlist_button.png");
    public static ImageIcon LightAddPlaylistButton = new ImageIcon("icons\\50x50\\light\\add_playlist_button.png");
    public static ImageIcon LightSongsButton = new ImageIcon("icons\\50x50\\light\\songs.png");
    public static ImageIcon LightAlbumsButton = new ImageIcon("icons\\50x50\\light\\albums.png");
    public static ImageIcon LightVolume0 = new ImageIcon("icons\\50x50\\light\\v0.png");
    public static ImageIcon LightVolume1 = new ImageIcon("icons\\50x50\\light\\v1.png");
    public static ImageIcon LightVolume2 = new ImageIcon("icons\\50x50\\light\\v2.png");
    public static ImageIcon LightVolume3 = new ImageIcon("icons\\50x50\\light\\v3.png");
    public static ImageIcon LightVolume4 = new ImageIcon("icons\\50x50\\light\\v4.png");
    public static ImageIcon LightUser = new ImageIcon("icons\\50x50\\light\\user.png");
    public static ImageIcon LightFriendsActivity = new ImageIcon("icons\\50x50\\light\\friends_activity.png");
    public static ImageIcon LightTitleLeft = new ImageIcon("icons\\50x50\\dark\\titleLeft.png");
    public static ImageIcon LightTitleRight = new ImageIcon("icons\\50x50\\dark\\titleRight.png");
    public static ImageIcon LightShare = new ImageIcon("icons\\50x50\\light\\share.png");
    public static ImageIcon Lightlike = new ImageIcon("icons\\50x50\\light\\like.png");
    public static ImageIcon LightShared = new ImageIcon("icons\\50x50\\light\\shared.png");
    public static ImageIcon Lightliked = new ImageIcon("icons\\50x50\\light\\liked.png");
    public static ImageIcon LightPartition = new ImageIcon("icons\\50x50\\light\\partiotion_vertical_line.png");
    public static ImageIcon LightSharedPlayListTitle = new ImageIcon("icons\\50x50\\light\\sharedPlaylistTitle.png");
    public static ImageIcon LightFavouritePlayListTitle = new ImageIcon("icons\\50x50\\light\\favouritePlayListTitle.png");
    public static ImageIcon LightNormalPlayListTitle = new ImageIcon("icons\\50x50\\light\\normalPlaylistTitle.png");
    public static ImageIcon LightFriendsActivitHorizentalPartition = new ImageIcon("icons\\50x50\\light\\friends activity horizental partition.png");
    public static ImageIcon LightAddFriend = new ImageIcon("icons\\50x50\\light\\addFriend.png");
    public static ImageIcon LightSongHorizentalPartition = new ImageIcon("icons\\50x50\\light\\song horizental partition.png");
    public static ImageIcon LightPlaySmall = new ImageIcon("icons\\50x50\\light\\playSmall.png");
    public static ImageIcon LightPauseSmall = new ImageIcon("icons\\50x50\\light\\pauseSmall.png");
    public static ImageIcon LightNoArtworkSmall = new ImageIcon("icons\\50x50\\light\\no_artwork-100x100.png");







    //dark mode:
    public static ImageIcon DarkShuffleOff = new ImageIcon("icons\\50x50\\dark\\shuffle_off.png");
    public static ImageIcon DarkShuffleOn = new ImageIcon("icons\\50x50\\dark\\shuffle_on.png");
    public static ImageIcon DarkPlay = new ImageIcon("icons\\50x50\\dark\\play.png");
    public static ImageIcon DarkNext = new ImageIcon("icons\\50x50\\dark\\next.png");
    public static ImageIcon DarkPrevious = new ImageIcon("icons\\50x50\\dark\\previous.png");
    public static ImageIcon DarkReplayAll = new ImageIcon("icons\\50x50\\dark\\replay_all.png");
    public static ImageIcon DarkVolume = new ImageIcon("icons\\50x50\\dark\\volume.png");
    public static ImageIcon DarkAdd = new ImageIcon("icons\\50x50\\dark\\add.png");
    public static ImageIcon DarkMusic = new ImageIcon("icons\\50x50\\dark\\music.png");
    public static ImageIcon DarkPause = new ImageIcon("icons\\50x50\\dark\\pause.png");
    public static ImageIcon DarkPlaylist = new ImageIcon("icons\\50x50\\dark\\playlist.png");
    public static ImageIcon DarkReplayOnce = new ImageIcon("icons\\50x50\\dark\\replay_once.png");
    public static ImageIcon DarkStop = new ImageIcon("icons\\50x50\\dark\\stop.png");
    public static ImageIcon DarkJpotifyLogo = new ImageIcon("icons\\50x50\\dark\\header.png");
    public static ImageIcon DarkNoArtwork = new ImageIcon("icons\\50x50\\dark\\no_artwork.png");
    public static ImageIcon DarkMagnifier = new ImageIcon("icons\\50x50\\dark\\magnifier.png");
    public static ImageIcon DarkPlaylistButton = new ImageIcon("icons\\50x50\\dark\\playlist_button.png");
    public static ImageIcon DarkAddPlaylistButton = new ImageIcon("icons\\50x50\\dark\\add_playlist_button.png");
    public static ImageIcon DarkSongsButton = new ImageIcon("icons\\50x50\\dark\\songs.png");
    public static ImageIcon DarkAlbumsButton = new ImageIcon("icons\\50x50\\dark\\albums.png");
    public static ImageIcon DarkVolume0 = new ImageIcon("icons\\50x50\\dark\\v0.png");
    public static ImageIcon DarkVolume1 = new ImageIcon("icons\\50x50\\dark\\v1.png");
    public static ImageIcon DarkVolume2 = new ImageIcon("icons\\50x50\\dark\\v2.png");
    public static ImageIcon DarkVolume3 = new ImageIcon("icons\\50x50\\dark\\v3.png");
    public static ImageIcon DarkVolume4 = new ImageIcon("icons\\50x50\\dark\\v4.png");
    public static ImageIcon DarkUser = new ImageIcon("icons\\50x50\\dark\\user.png");
    public static ImageIcon DarkFriendsActivity = new ImageIcon("icons\\50x50\\dark\\friends_activity.png");
    public static ImageIcon DarkTitleLeft = new ImageIcon("icons\\50x50\\dark\\titleLeft.png");
    public static ImageIcon DarkTitleRight = new ImageIcon("icons\\50x50\\dark\\titleRight.png");
    public static ImageIcon DarkShare = new ImageIcon("icons\\50x50\\dark\\share.png");
    public static ImageIcon Darklike = new ImageIcon("icons\\50x50\\dark\\like.png");
    public static ImageIcon DarkShared = new ImageIcon("icons\\50x50\\dark\\shared.png");
    public static ImageIcon Darkliked = new ImageIcon("icons\\50x50\\dark\\liked.png");
    public static ImageIcon DarkPartition = new ImageIcon("icons\\50x50\\dark\\partiotion_vertical_line.png");
    public static ImageIcon DarkSharedPlayListTitle = new ImageIcon("icons\\50x50\\dark\\sharedPlaylistTitle.png");
    public static ImageIcon DarkFavouritePlayListTitle = new ImageIcon("icons\\50x50\\dark\\favouritePlayListTitle.png");
    public static ImageIcon DarkNormalPlayListTitle = new ImageIcon("icons\\50x50\\dark\\normalPlaylistTitle.png");
    public static ImageIcon DarkFriendsActivitHorizentalPartition = new ImageIcon("icons\\50x50\\dark\\friends activity horizental partition.png");
    public static ImageIcon DarkAddFriend = new ImageIcon("icons\\50x50\\dark\\addFriend.png");
    public static ImageIcon DarkSongHorizentalPartition = new ImageIcon("icons\\50x50\\dark\\song horizental partition.png");
    public static ImageIcon DarkPlaySmall = new ImageIcon("icons\\50x50\\dark\\playSmall.png");
    public static ImageIcon DarkPauseSmall = new ImageIcon("icons\\50x50\\dark\\pauseSmall.png");
    public static ImageIcon DarkNoArtworkSmall = new ImageIcon("icons\\50x50\\dark\\no_artwork-100x100.png");















}
