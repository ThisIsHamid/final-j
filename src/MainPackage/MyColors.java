package MainPackage;

import java.awt.*;


public class MyColors {

    public static Color NormalGray = new Color(127,127,127);

    public static Color Trancparent = new Color(0f,0f,0f,.0f );



    public static Color LightFooter = new Color(205,205,205);
    public static Color DarkFooter = new Color(50,50,50);

    public static Color LightBackground = new Color(245,245,245);
    public static Color DarkBackground = new Color(10,10,10);

    public static Color LightLeftBar = new Color(220,220,220);
    public static Color DarkLeftBar = new Color(35,35,35);

    public static Color LightRightBar = new Color(213,213,213);
    public static Color DarkRightBar = new Color(42,42,42);

    public static Color LightTextColor = new Color(33,112,255);
    public static Color DarkTextColor = new Color(195,133,0);

    public static Color LighterTextColor = new Color(33,112,255);
    public static Color DarkerTextColor = new Color(107,73,0);

    public static Color LightMenu = new Color(210,210,210);
    public static Color DarkMenu = new Color(25,25,25);
}
