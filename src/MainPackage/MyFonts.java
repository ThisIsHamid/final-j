package MainPackage;

import java.awt.*;

public class MyFonts {
    public static Font arial = new Font("Arial Rounded MT Bold",Font.PLAIN, 14);
    public static Font arialBold = new Font("Arial Rounded MT Bold",Font.PLAIN, 16);
    public static Font heavyTitle = new Font("Franklin Gothic Heavy",Font.PLAIN, 50);
    public static Font cursiveTitle = new Font("Mistral",Font.PLAIN, 35);
    public static Font cursiveTitleSmall = new Font("Mistral",Font.PLAIN, 15);
    public static Font arialForPlaylistButtons = new Font("Arial Rounded MT Bold",Font.PLAIN, 12);


}
